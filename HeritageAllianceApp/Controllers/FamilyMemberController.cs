﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HeritageAllianceApp.Models;

namespace HeritageAllianceApp.Controllers
{
    // CRUD for any family members related to deceased.

    public class FamilyMemberController : Controller
    {
        private HeritageAllianceAppDb _db = new HeritageAllianceAppDb();

        //
        // GET: /FamilyMember/

        public ActionResult Index()
        {
            var familymembers = _db.FamilyMembers.Include(f => f.Deceased);
            return View(familymembers.ToList());
        }

        //
        // GET: /FamilyMember/Details/5

        public ActionResult Details(int did, int id = 0)
        {
            FamilyMember familymember = _db.FamilyMembers.Find(id);
            if (familymember == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(familymember);
        }

        //
        // GET: /FamilyMember/Create

        public ActionResult Create(int did)
        {
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View();
        }

        //
        // POST: /FamilyMember/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FamilyMember familymember, int did)
        {
            if (ModelState.IsValid)
            {
                _db.FamilyMembers.Add(familymember);
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }

            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View(familymember);
        }

        //
        // GET: /FamilyMember/Edit/5

        public ActionResult Edit(int did, int id = 0)
        {
            FamilyMember familymember = _db.FamilyMembers.Find(id);
            if (familymember == null)
            {
                return HttpNotFound();
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View(familymember);
        }

        //
        // POST: /FamilyMember/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FamilyMember familymember, int did)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(familymember).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View(familymember);
        }

        //
        // GET: /FamilyMember/Delete/5

        public ActionResult Delete(int did, int id = 0)
        {
            FamilyMember familymember = _db.FamilyMembers.Find(id);
            if (familymember == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(familymember);
        }

        //
        // POST: /FamilyMember/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int did)
        {
            FamilyMember familymember = _db.FamilyMembers.Find(id);
            _db.FamilyMembers.Remove(familymember);
            _db.SaveChanges();
            return RedirectToAction("Details", "Deceased", new { id = did });
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}