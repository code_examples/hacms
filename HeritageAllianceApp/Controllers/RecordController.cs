﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HeritageAllianceApp.Models;

namespace HeritageAllianceApp.Controllers
{
    // Crud for records. See ERD.
    public class RecordController : Controller
    {
        private HeritageAllianceAppDb _db = new HeritageAllianceAppDb();

        //
        // GET: /Record/

        public ActionResult Index()
        {
            var records = _db.Records.Include(r => r.Deceased);
            return View(records.ToList());
        }

        //
        // GET: /Record/Details/5

        public ActionResult Details(int did, int id = 0)
        {
            Record record = _db.Records.Find(id);
            if (record == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(record);
        }

        //
        // GET: /Record/Create

        public ActionResult Create(int did)
        {
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View();
        }

        //
        // POST: /Record/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Record record, int did)
        {
            if (ModelState.IsValid)
            {
                _db.Records.Add(record);
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }

            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View(record);
        }

        //
        // GET: /Record/Edit/5

        public ActionResult Edit(int did, int id = 0)
        {
            Record record = _db.Records.Find(id);
            if (record == null)
            {
                return HttpNotFound();
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View(record);
        }

        //
        // POST: /Record/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Record record, int did)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(record).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            return View(record);
        }

        //
        // GET: /Record/Delete/5

        public ActionResult Delete(int did, int id = 0)
        {
            Record record = _db.Records.Find(id);
            if (record == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(record);
        }

        //
        // POST: /Record/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int did, int id)
        {
            Record record = _db.Records.Find(id);
            _db.Records.Remove(record);
            _db.SaveChanges();
            return RedirectToAction("Details", "Deceased", new { id = did });
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}