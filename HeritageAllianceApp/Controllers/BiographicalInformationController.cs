﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HeritageAllianceApp.Models;

namespace HeritageAllianceApp.Controllers
{
    // This controller handles CRUD for information about a specific person. See ERD.
    public class BiographicalInformationController : Controller
    {
        private HeritageAllianceAppDb _db = new HeritageAllianceAppDb();

        //
        // GET: /BiographicalInformation/

        public ActionResult Index()
        {
            var biographicalinformation = _db.BiographicalInformation.Include(b => b.Deceased).Include(b => b.FamilyMember);
            return View(biographicalinformation.ToList());
        }

        //
        // GET: /BiographicalInformation/Details/5

        public ActionResult Details(int did, int id = 0)
        {
            BiographicalInformation biographicalinformation = _db.BiographicalInformation.Find(id);
            if (biographicalinformation == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(biographicalinformation);
        }

        //
        // GET: /BiographicalInformation/Create

        public ActionResult Create(int did)
        {
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            ViewBag.FamilyMemberId = new SelectList(_db.FamilyMembers, "FamilyMemberId", "Relationship");
            return View();
        }

        //
        // POST: /BiographicalInformation/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BiographicalInformation biographicalinformation, int did)
        {
            if (ModelState.IsValid)
            {
                _db.BiographicalInformation.Add(biographicalinformation);
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }

            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            ViewBag.FamilyMemberId = new SelectList(_db.FamilyMembers, "FamilyMemberId", "Relationship", biographicalinformation.FamilyMemberId);
            return View(biographicalinformation);
        }

        //
        // GET: /BiographicalInformation/Edit/5

        public ActionResult Edit(int did, int id = 0)
        {
            BiographicalInformation biographicalinformation = _db.BiographicalInformation.Find(id);
            if (biographicalinformation == null)
            {
                return HttpNotFound();
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            ViewBag.FamilyMemberId = new SelectList(_db.FamilyMembers, "FamilyMemberId", "Relationship", biographicalinformation.FamilyMemberId);
            return View(biographicalinformation);
        }

        //
        // POST: /BiographicalInformation/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BiographicalInformation biographicalinformation, int did)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(biographicalinformation).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.did = did;
            ViewBag.FamilyMemberId = new SelectList(_db.FamilyMembers, "FamilyMemberId", "Relationship", biographicalinformation.FamilyMemberId);
            return View(biographicalinformation);
        }

        //
        // GET: /BiographicalInformation/Delete/5

        public ActionResult Delete(int did, int id = 0)
        {
            BiographicalInformation biographicalinformation = _db.BiographicalInformation.Find(id);
            if (biographicalinformation == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(biographicalinformation);
        }

        //
        // POST: /BiographicalInformation/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int did)
        {
            BiographicalInformation biographicalinformation = _db.BiographicalInformation.Find(id);
            _db.BiographicalInformation.Remove(biographicalinformation);
            _db.SaveChanges();
            return RedirectToAction("Details", "Deceased", new { id = did });
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}