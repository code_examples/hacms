﻿using HeritageAllianceApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HeritageAllianceApp.Controllers
{
    // Handles all search requests.
    public class SearchController : Controller
    {
        // Initialize DB.
        private HeritageAllianceAppDb _db = new HeritageAllianceAppDb();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search()
        {   // Input validation. Needs to be more extensive later.         
            if (Request.Form["cname"] != null)
            {                
                return RedirectToAction("Cemeteries");
            }
            else if (Request.Form["fname"] != null ||
                     Request.Form["mname"] != null ||
                     Request.Form["lname"] != null ||
                     Request.Form["dob"] != null ||
                     Request.Form["dod"] != null)
            {
                
                return RedirectToAction("Deceased");
            }
            else
            {
                return View();
            }
            
        }

        public ActionResult Cemeteries()
        {
            // Get the list of cemetaries based on search.
            string cname = Request.Form["cname"];
            List<Cemetery> cems = _db.Cemeteries
                                     .Where(c => c.CemeteryName.Contains(cname) || cname == string.Empty)
                                     .ToList();
            return View(cems);
        }
        // List deceased based on search.
        public ActionResult Deceased()
        {
            string fname = Request.Form["fname"];
            string mname = Request.Form["mname"];
            string lname = Request.Form["lname"];
            string dob = Request.Form["dob"];
            string dod = Request.Form["dod"];
            List<Deceased> decs = _db.Deceased
                                         .Where(d => d.FirstName.ToLower().Contains(fname.ToLower()) || fname == string.Empty)
                                         .Where(d => d.MiddleName.ToLower().Contains(mname.ToLower()) || mname == string.Empty)
                                         .Where(d => d.LastName.ToLower().Contains(lname.ToLower()) || lname == string.Empty)
                                         .Where(d => d.DateOfBirth.ToLower().Contains(dob.ToLower()) || dob == string.Empty)
                                         .Where(d => d.DateOfDeath.ToLower().Contains(dod.ToLower()) || dod == string.Empty)
                                         .ToList();
            return View(decs);
        }

        // Overwrite garbage collection. Best practice. 
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }

    }
}
