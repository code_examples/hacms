﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HeritageAllianceApp.Models;

namespace HeritageAllianceApp.Controllers
{
    // Crud to handle relations between deceased.
    public class RelatedController : Controller
    {
        private HeritageAllianceAppDb _db = new HeritageAllianceAppDb();

        public ActionResult Index()
        {
            var related = _db.Related.Include(r => r.Deceased).Include(r => r.DeceasedRelative);
            return View(related.ToList());
        }

        public ActionResult Admin()
        {            
            return View(_db.Related.ToList());
        }

        public ActionResult Details(int id = 0)
        {
            Related related = _db.Related.Find(id);
            if (related == null)
            {
                return HttpNotFound();
            }
            return View(related);
        }

        public ActionResult Create(int did)
        {
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.DeceasedRelativeId = new SelectList(deceased, "DeceasedId", "DeceasedName");
            ViewBag.did = did;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Related related, int did)
        {
            if (ModelState.IsValid)
            {
                _db.Related.Add(related);
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }

            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName");
            ViewBag.DeceasedRelativeId = new SelectList(deceased, "DeceasedId", "DeceasedName");
            ViewBag.did = did;
            return View(related);
        }

        public ActionResult Edit(int did, int rid, int id = 0)
        {
            Related related = _db.Related.Find(id);
            if (related == null)
            {
                return HttpNotFound();
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName", did);
            ViewBag.DeceasedRelativeId = new SelectList(deceased, "DeceasedId", "DeceasedName", rid);
            ViewBag.did = did;
            return View(related);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Related related, int did)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(related).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", "Deceased", new { id = did });
            }
            var deceased = _db.Deceased
                               .ToList()
                               .Select(d => new
                               {
                                   DeceasedId = d.DeceasedId,
                                   DeceasedName = string.Format("{0} {1} {2}", d.FirstName, d.MiddleName, d.LastName)
                               });
            ViewBag.DeceasedId = new SelectList(deceased, "DeceasedId", "DeceasedName");
            ViewBag.DeceasedRelativeId = new SelectList(deceased, "DeceasedId", "DeceasedName");
            ViewBag.did = did;
            return View(related);
        }

        public ActionResult Delete(int did, int id = 0)
        {
            Related related = _db.Related.Find(id);
            if (related == null)
            {
                return HttpNotFound();
            }
            ViewBag.did = did;
            return View(related);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int did, int id)
        {
            Related related = _db.Related.Find(id);
            _db.Related.Remove(related);
            _db.SaveChanges();
            return RedirectToAction("Details", "Deceased", new { id = did });
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}