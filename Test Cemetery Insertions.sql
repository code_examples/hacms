﻿INSERT INTO Deceased (First_Name, Middle_Name, Last_Name, Date_Of_Birth, Date_Of_Death, Row_Number, Location_Within_Row, Stone_Description, Type_Of_Burial, Cemetery_Id)
VALUES ('Josh', 'A', 'Bowers', '1981 09 11', '2015 04 20', '5', '20', 'Stone', 'In-Ground', 2)

INSERT INTO Deceased (First_Name, Middle_Name, Last_Name, Date_Of_Birth, Date_Of_Death, Row_Number, Location_Within_Row, Stone_Description, Type_Of_Burial, Cemetery_Id)
VALUES ('Priscilla', 'A', 'Trent', '1978 03 03', '2015 04 20', '5', '21', 'Stone', 'In-Ground', 2)

INSERT INTO Related (Deceased_Id, Deceased_Relative_Id, Relationship)
VALUES (160, 161, 'Wife')

INSERT INTO Related (Deceased_Id, Deceased_Relative_Id, Relationship)
VALUES (161, 160, 'Husband')

INSERT INTO FamilyMember (First_Name, Middle_Name, Last_Name, Date_Of_Birth, Relationship, Deceased_Id)
VALUES ('Lucas', 'T', 'Bowers', '2008 07 14', 'Son', 160)

INSERT INTO FamilyMember (First_Name, Middle_Name, Last_Name, Date_Of_Birth, Relationship, Deceased_Id)
VALUES ('Lucas', 'T', 'Bowers', '2008 07 14', 'Son', 161)

INSERT INTO Record(Record_Type, Record_Description, Date_Entered, Entered_By, Deceased_Id)
VALUES ('Deed', 'Deed to house', 04/20/2015, 'Record Keeper', 160)

INSERT INTO Record(Record_Type, Record_Description, Date_Entered, Entered_By, Deceased_Id)
VALUES ('Warrant', 'Warrant for Josh''s Arrest', 04/20/2015, 'Record Keeper', 160)

INSERT INTO BiographicalInformation(Biographical_Information_Type, Biographical_Information_Description, Date_Entered, Entered_By, Deceased_Id)
VALUES ('Image', 'Here is a picture of Josh Bowers', 04/20/2015, 'Biographer', 160)

INSERT INTO BiographicalInformation(Biographical_Information_Type, Biographical_Information_Description, Date_Entered, Entered_By, Deceased_Id)
VALUES ('Music Video', 'Josh loved this song', 04/20/2015, 'Biographer', 160)

INSERT INTO RecordLink(URL, Link_To, Record_Id)
VALUES ('http://www.google.com', 'Google Homepage', 1)

INSERT INTO InformationLink(URL, Link_To, Information_Id)
VALUES ('https://www.youtube.com/watch?v=6WTdTwcmxyo', 'Tommy Tutone - 867-5309 (Jenny)', 2)

